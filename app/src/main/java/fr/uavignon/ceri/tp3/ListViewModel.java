package fr.uavignon.ceri.tp3;

import android.app.Application;
import android.webkit.WebBackForwardList;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;

import java.util.List;

import fr.uavignon.ceri.tp3.data.City;
import fr.uavignon.ceri.tp3.data.WeatherRepository;

public class ListViewModel extends AndroidViewModel {
    private WeatherRepository repository;
    private LiveData<List<City>> allCities;
    private MutableLiveData<Boolean> isLoading;
    private MutableLiveData<Throwable> webServiceThrowable;

    public ListViewModel (Application application) {
        super(application);
        repository = WeatherRepository.get(application);
        allCities = repository.getAllCities();
        isLoading = repository.getIsLoading();
        webServiceThrowable = new MutableLiveData<>();
    }

    LiveData<List<City>> getAllCities() {
        return allCities;
    }

    MutableLiveData<Boolean> getIsLoading() {
        return isLoading;
    }

    MutableLiveData<Throwable> getWebServiceThrowable() {
        return webServiceThrowable;
    }

    public void setWebServiceThrowable(Throwable throwable) {
        webServiceThrowable.postValue(throwable);
    }

    public void deleteCity(long id) {
        repository.deleteCity(id);
    }


}
