package fr.uavignon.ceri.tp3.data.webservice;

public class WeatherRequestStatus {
    public final boolean isLoading;
    public  final Throwable error;

    public WeatherRequestStatus(boolean isLoading, Throwable error) {
        this.error = error;
        this.isLoading = isLoading;
    }

    boolean isDone() {
        return !isLoading && (error == null);
    }
}
