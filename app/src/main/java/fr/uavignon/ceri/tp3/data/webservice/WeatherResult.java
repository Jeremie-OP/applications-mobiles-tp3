package fr.uavignon.ceri.tp3.data.webservice;

import java.util.List;

import fr.uavignon.ceri.tp3.data.City;

public class WeatherResult {
    public final boolean isLoading;
    public final List<Forecast> forecasts;
    public final Throwable error;

    public WeatherResult(boolean isLoading, List<Forecast> forecasts, Throwable error) {
        this.isLoading = isLoading;
        this.forecasts = forecasts;
        this.error = error;
    }

    static public void transferInfo(WeatherResponse weatherInfo, City cityInfo) {
        final Forecast forecast =  weatherInfo.weather.get(0);

        cityInfo.setTemperature((float) weatherInfo.main.temperature);
        cityInfo.setHumidity((int) weatherInfo.main.humidity);
        cityInfo.setWindSpeed((int) weatherInfo.wind.speed);
        cityInfo.setWindDirection((float) weatherInfo.wind.deg);
        cityInfo.setCloudiness((int) weatherInfo.clouds.cloudiness);
        cityInfo.setIcon(forecast.icon);
    }
}
