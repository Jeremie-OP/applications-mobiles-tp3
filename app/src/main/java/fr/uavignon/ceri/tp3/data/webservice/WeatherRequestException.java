package fr.uavignon.ceri.tp3.data.webservice;

public class WeatherRequestException extends Exception {
    public WeatherRequestException(String message) {
        super(message);
    }
}
