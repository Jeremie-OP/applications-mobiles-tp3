package fr.uavignon.ceri.tp3.data.webservice;


public class Forecast {
    public final String name;
    public String icon;

    public Forecast(String name, int temperature, String temperatureUnit, String icon) {
        this.name = name;
        this.icon = icon;
    }
}
