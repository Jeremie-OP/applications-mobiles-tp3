package fr.uavignon.ceri.tp3.data.webservice;

import com.squareup.moshi.Json;

import java.util.List;


public class WeatherResponse {
    public final Properties properties=null;

    public static class Properties {
        public final List<Forecast> periods=null;
    }

    public List<Forecast> weather;

    public String base;
    public @Json(name = "cod") String cod;

    public static class Main {
        public @Json(name="temp") double temperature;
        public @Json(name="feels_like") double feelsLike;
        public @Json(name="temp_min") double tempMin;
        public @Json(name="temp_max") double tempMax;
        public double pressure;
        public double humidity;
    }
    public Main main;

    public static class Wind {
        public @Json(name = "speed") double speed;
        public @Json(name = "deg") int deg;
    }

    public Wind wind;

    public static class Clouds {
        public @Json(name = "all") double cloudiness;
    }

    public Clouds clouds;

}

